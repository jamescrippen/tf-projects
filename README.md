### Flowchart
```mermaid
graph LR
A[Hard Edge] -->B(Round Edge)
    B --> C{Desicion}
    C -->|One| D[Result One]
    C -->|Two| E[Result Two]
```