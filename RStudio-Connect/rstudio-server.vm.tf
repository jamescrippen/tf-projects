resource "azurerm_network_interface" "vmnic0-server" {
  name                  = "${var.vm_name_server}_NIC1"
  location              = "${var.azure_location}"
  resource_group_name   = "${var.resource_group_name}"
  network_security_group_id = "${azurerm_network_security_group.nsg.id}"

  ip_configuration {
    name                = "ipconfig_${var.vm_name_server}"
    subnet_id           = "/subscriptions/${var.azure_subscription_id}/resourceGroups/${var.vnet_rsgroup_name}/providers/Microsoft.Network/virtualNetworks/${var.virtualnetworkname}/subnets/${var.subnet_name}"
    private_ip_address_allocation = "dynamic"
  }

}

resource "azurerm_virtual_machine" "rstudioserver" {
    name                  = "${var.vm_name_server}"
    location              = "${var.azure_location}"
    resource_group_name   = "${azurerm_resource_group.default.name}"
    network_interface_ids = ["${azurerm_network_interface.vmnic0-server.id}"]
    vm_size               = "Standard_D2s_v3"

    storage_os_disk {
        name              = "${var.vm_name_server}_OsDisk_1_${random_id.randomId.hex}"
        caching           = "ReadWrite"
        create_option     = "FromImage"
        managed_disk_type = "Premium_LRS"
    }

    delete_os_disk_on_termination = true

    storage_image_reference {
        publisher = "OpenLogic"
        offer     = "CentOS"
        sku       = "7.5"
        version   = "latest"
    }

    os_profile {
        computer_name  = "${var.hostname_server}"
        admin_username = "azureadmin"
        admin_password = "${var.admin_password}"
    }

    os_profile_linux_config {
        disable_password_authentication = false
        ssh_keys {
            path = "/home/azureadmin/.ssh/authorized_keys"
            key_data = "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAjX+aOl3CxlEN8ceHQRvBS9RhdfM5+McdJhA0NX6NtPscax97+3afnQd+do+DszoKKm8/ESPvyyta0jDtY2nAcfe+5H8jh7ectG0IUzByH6I3h29olwJUPl7jwep6y5z2kuimcfylz9d5GoOUSuBui3D53v0lJgWGkLnYWmKfL7C3AXxALMvsXG1IpWbaWsDhIippq8gpMEui91Q1wvay3S+vaqPkRzH2DCBtYDhAypwatuvcWEJxKISPNq+DfvH3JUS2/dBoHT2GSuLYeLBSiyySNpCSOOKsDNTzftZlo0Ju3imR3aFaxX11n+SmJaaoE3hA8o6YmfB/yymqeMUkeQ== rsa-key-20180821"
        }
    }

    boot_diagnostics {
        enabled     = "true"
        storage_uri = "${azurerm_storage_account.mystorageaccount.primary_blob_endpoint}"
    }

    tags {
        environment = "RStudio Connect Test"
        project = "BBOPS - Project27"
    }

    provisioner "remote-exec" {
      connection = {
          user      = "azureadmin"
          password  = "${var.admin_password}"
          agent     = "false"
          insecure  = "true"
      }
      inline = [
        "echo ${var.admin_password} | sudo -S yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm",
        "sudo yum install -y libssl-devel openssl-devel openssl-libs git libgit2-devel openldap-clients python swig numpy patch",
        "sudo sed -i 's/enforcing/disabled/g' /etc/sysconfig/selinux",
        "sudo yum-builddep -y R",
        "sudo yum install -y R",
      ]
    }

    depends_on = ["azurerm_subnet.private"]
}