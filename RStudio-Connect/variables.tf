variable "azure_subscription_id" {
  description = "Azure Subscription ID"
}

variable "azure_client_id" {
  description = "Azure Client ID"
}

variable "azure_client_secret" {
  description = "Azure Client Secret"
}

variable "azure_tenant_id" {
  description = "Azure Tenant ID"
}

variable "azure_location" {
  description = "Azure Location, e.g. South Central US"
  default = "South Central US"
}

variable "virtualnetworkname" {
  description = "Name of the virtual network"
  default = "BBOPS-VNet01"
}

variable "resource_group_name" {
  description = "Azure Resource Group Name"
  default = "BBOPS-P27-RG"
}

variable "vnet_rsgroup_name" {
  description = "Virtual Network Resource Group Name"
  default = "BBOPS-Network-RG"
}

variable "cidr" {
  description = "CIDR range of the VNET"
  default = "10.200.16.0/20"
}

variable "nsg_name" {
  description = "Name of Network Security Rule"
  default = "AllowSSH"
}

variable "vm_name_connect" {
    description = "Name of Virtual Machine"
    default = "BBOPS-RSCONNECT-01"
}

variable "vm_name_server" {
  description = "Name of Server virtual machine"
  default = "BBOPS-RSERVER-01"
}
variable "hostname_connect" {
  description = "Hostname os Virtual Machine"
  default = "rsconnect01"
}

variable "hostname_server" {
  description = "Hostname of Server virtual machine"
  default = "rsserver01"
}

variable "subnet_name" {
  description = "Default Subnet Name"
  default = "BBOPS-P27-PRIVATE"
}

variable "shrdstor_account" {
  description = "Name of storage account used for diagnostics"
  default = "bbopsp27shrd"
}

variable "bind_password" {
  description = "Password used in LDAP bind string"
  default = "XD5UZyddRlB6PzZgKiNAcQo="
}

variable "admin_password" {
  description = "Linux admin password"
}