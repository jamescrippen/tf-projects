provider "azurerm" {
  subscription_id       = "${var.azure_subscription_id}"
  client_id             = "${var.azure_client_id}"
  client_secret         = "${var.azure_client_secret}"
  tenant_id             = "${var.azure_tenant_id}"
}

resource "azurerm_resource_group" "default" {
  name			        = "${var.resource_group_name}"
  location		        = "${var.azure_location}"
}

resource "azurerm_virtual_network" "default" {
  name			        = "${var.virtualnetworkname}"
  address_space		    = ["${var.cidr}"]
  location		        = "${var.azure_location}"
  resource_group_name	= "${var.vnet_rsgroup_name}"
  #dns_servers           = ["10.0.12.4","10.1.100.152"]
}

resource "azurerm_subnet" "private" {
    name                = "${var.subnet_name}"
    resource_group_name = "${var.vnet_rsgroup_name}"
    virtual_network_name = "${var.virtualnetworkname}"
    address_prefix      = "10.200.16.0/24"
    service_endpoints   = ["Microsoft.Sql"]
}

resource "azurerm_network_security_group" "nsg" {
  name                  = "${var.nsg_name}"
  location              = "${var.azure_location}"
  resource_group_name   = "${var.resource_group_name}"
  security_rule {
    name                = "SSH"
    priority            = "1001"
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "random_id" "randomId" {
    keepers = {
        # Generate a new ID only when a new resource group is defined
        resource_group  = "${var.resource_group_name}"
    }

    byte_length         = 8
}

resource "azurerm_storage_account" "mystorageaccount" {
    name                = "${var.shrdstor_account}"
    resource_group_name = "${var.resource_group_name}"
    location            = "eastus"
    account_replication_type = "LRS"
    account_tier        = "Standard"
}

